package com.kasidis.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileManagerTest {
    @Test
    public void readFileByLine() {
        FileManager fileManager = new FileManager();
        List<String> outputList;
        List<String> expectedList = new ArrayList<>(Arrays.asList("Ant", "Boy", "Cat", "Dog", "Egg", "Fan"));
        outputList = fileManager.readFileByLine("input/util_test_001.txt");
        Assert.assertEquals(expectedList, outputList);

        outputList = fileManager.readFileByLine("input/problem_two_001.txt");
        String outputString = outputList.get(2);
        String expectedString = "Lua for the Masses 30min";
        Assert.assertEquals(outputString, expectedString);
    }

    @Test
    public void readFileByCommaAndWhiteSpace() {
        FileManager fileManager = new FileManager();
        List<String> outputList;
        List<String> expectedList;
        outputList = fileManager.readFileByCommaAndWhiteSpace("input/problem_one_001.txt");
        expectedList = new ArrayList<>(Arrays.asList("AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"));
        Assert.assertEquals(expectedList, outputList);
    }
}