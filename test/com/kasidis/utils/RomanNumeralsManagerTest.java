package com.kasidis.utils;

import com.kasidis.CustomException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralsManagerTest {

    RomanNumeralsManager romanNumeralsManager;

    @Before
    public void init() {
        romanNumeralsManager = new RomanNumeralsManager();
    }

    @Test
    public void romanNumeralsToValue() {
        String input;
        int output, expected;

        input = "XLII";
        try {
            output = romanNumeralsManager.romanNumeralsToValue(input);
            expected = 42;
            assertEquals(expected, output);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = "";
        try {
            output = romanNumeralsManager.romanNumeralsToValue(input);
            expected = 0;
            assertEquals(expected, output);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = "IIZI";
        try {
            output = romanNumeralsManager.romanNumeralsToValue(input);
            expected = 4;
            assertEquals(expected, output);
        } catch (Exception e) {
            CustomException customException = new CustomException();
            assertEquals(e.getMessage(), customException.romanNumeralsContainUnexpectedValue.getMessage());
        }
    }

    @Test
    public void valueToRomanNumerals() {
        int input;
        String output,expected ;

        input = 1903;
        expected = "MCMIII";
        output = romanNumeralsManager.valueToRomanNumerals(input);
        assertEquals(expected, output);

        input = 888;
        expected = "DCCCLXXXVIII";
        output = romanNumeralsManager.valueToRomanNumerals(input);
        assertEquals(expected, output);

    }

    @Test
    public void digitValueToRomanNumeral() {
        int input;
        String output, expected;

        input = 800;
        output = romanNumeralsManager.digitValueToRomanNumeral(input);
        expected = "DCCC";
        assertEquals(expected, output);

        input = 1000;
        output = romanNumeralsManager.digitValueToRomanNumeral(input);
        expected = "M";
        assertEquals(expected, output);

        input = 900;
        output = romanNumeralsManager.digitValueToRomanNumeral(input);
        expected = "CM";
        assertEquals(expected, output);

        input = 3;
        output = romanNumeralsManager.digitValueToRomanNumeral(input);
        expected = "III";
        assertEquals(expected, output);
    }
}