package com.kasidis.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListManagerTest {

    @Test
    public void findMatchInArrayList() {
        ArrayListManager arrayListManager = new ArrayListManager();
        List<String> inputList = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        String inputSearchElement = "AB";
        String output;
        output = arrayListManager.findMatchInArrayList(inputList,inputSearchElement);
        String expected = "AB5";
        Assert.assertEquals(expected, output);

        inputSearchElement = "DG";
        output = arrayListManager.findMatchInArrayList(inputList,inputSearchElement);
        expected = "";
        Assert.assertEquals(expected, output);
    }
}