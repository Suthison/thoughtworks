package com.kasidis.utils;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateManagerTest {

    @Test
    public void addMinutesToJavaUtilDate() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mma");
        String inputDateString, expectedDateString;
        Date inputDate, outputDate, expectedDate;
        int inputMinutes;
        DateManager dateManager = new DateManager();
        inputDateString = "09:00AM";
        expectedDateString = "09:30AM";
        inputMinutes = 30;
        try {
            inputDate = dateFormat.parse(inputDateString);
            expectedDate = dateFormat.parse(expectedDateString);
            outputDate = dateManager.addMinutesToJavaUtilDate(inputDate, inputMinutes);
            Assert.assertEquals(expectedDate, outputDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        inputDateString = "12:10AM";
        expectedDateString = "00:55AM";
        inputMinutes = 45;
        try {
            inputDate = dateFormat.parse(inputDateString);
            expectedDate = dateFormat.parse(expectedDateString);
            outputDate = dateManager.addMinutesToJavaUtilDate(inputDate, inputMinutes);
            Assert.assertEquals(expectedDate, outputDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}