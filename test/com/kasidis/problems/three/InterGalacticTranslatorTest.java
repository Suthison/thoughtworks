package com.kasidis.problems.three;

import com.kasidis.CustomException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.kasidis.CustomException.*;
import static org.junit.Assert.*;

public class InterGalacticTranslatorTest {

    InterGalacticTranslator interGalacticTranslator;

    @Before
    public void init() {
        interGalacticTranslator = new InterGalacticTranslator();
    }

    @Test
    public void messageTranslate() {
        String inputMessage;
        String output, expected;

        ProductObject gold = new ProductObject("Gold",5);
        List<ProductObject> products = new ArrayList<>();
        products.add(gold);
        interGalacticTranslator.setProducts(products);

        List<InterGalacticNumeralObject> interGalacticNumerals = new ArrayList<>();
        interGalacticNumerals.add(new InterGalacticNumeralObject("glob","I"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("prok","V"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("pish","X"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("tegj","L"));

        interGalacticTranslator.setInterGalacticNumerals(interGalacticNumerals);


        inputMessage = "how much is pish tegj glob glob ?";
        output = interGalacticTranslator.messageTranslate(inputMessage);
        expected = "pish tegj glob glob is 42";
        assertEquals(expected, output);

        inputMessage = "how much is tegj pish pish pish prok glob glob glob ?";
        output = interGalacticTranslator.messageTranslate(inputMessage);
        expected = "tegj pish pish pish prok glob glob glob is 88";
        assertEquals(expected, output);

        inputMessage = "how many Credits is glob prok Gold ?";
        output = interGalacticTranslator.messageTranslate(inputMessage);
        expected = "glob prok Gold is 20 credits";
        assertEquals(expected, output);

        inputMessage = "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?";
        output = interGalacticTranslator.messageTranslate(inputMessage);
        expected = "I have no idea what you are talking about";
        assertEquals(expected, output);


    }

    @Test
    public void setInterGalacticToRomanNumerals() {
        String inputInterGalactic, inputRoman;
        InterGalacticNumeralObject output,expected;

        inputInterGalactic = "glob";
        inputRoman = "I";
        expected = new InterGalacticNumeralObject("glob","I");
        try {
            output = interGalacticTranslator.setInterGalacticToRomanNumerals(inputInterGalactic, inputRoman);
            assertEquals(expected.getInterGalacticNumeral() , output.getInterGalacticNumeral());
            assertEquals(expected.getRomanNumeral() , output.getRomanNumeral());
        } catch (Exception e) {
            e.printStackTrace();
        }

        inputInterGalactic = "glob";
        inputRoman = "Z";
        try {
            output = interGalacticTranslator.setInterGalacticToRomanNumerals(inputInterGalactic, inputRoman);
        } catch (Exception e) {
            CustomException customException = new CustomException();
            assertEquals(e.getMessage(),romanNumeralsNotExist.getMessage());
        }
    }


    @Test
    public void setProductValueEach() {
        String input;
        List<ProductObject> output, expected = new ArrayList<>();

        List<InterGalacticNumeralObject> interGalacticNumerals = new ArrayList<>();
        interGalacticNumerals.add(new InterGalacticNumeralObject("glob","I"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("prok","V"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("pish","X"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("tegj","L"));

        interGalacticTranslator.setInterGalacticNumerals(interGalacticNumerals);

        input = "glob glob Silver is 34 Credits";
        try {
            interGalacticTranslator.setProductValueEach(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        output = interGalacticTranslator.getProducts();
        expected.add(new ProductObject("Silver",17));
        assertEquals(expected.get(0).getProductName(),output.get(0).getProductName());
        assertEquals(expected.get(0).getProductValue(),output.get(0).getProductValue(),0.1);

        input = "glob glob Silver is 20 Credits";
        try {
            interGalacticTranslator.setProductValueEach(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        output = interGalacticTranslator.getProducts();
        expected = new ArrayList<>();
        expected.add(new ProductObject("Silver",10));
        assertEquals(expected.get(0).getProductName(),output.get(0).getProductName());
        assertEquals(expected.get(0).getProductValue(),output.get(0).getProductValue(),0.1);

        input = "glob globs Silver is 20 Credits";
        try {
            interGalacticTranslator.setProductValueEach(input);
        } catch (Exception e) {
            assertEquals(e.getMessage(),interGalacticNumeralNotMatch.getMessage());
        }

        input = "Silver is 4 Credits";
        try {
            interGalacticTranslator.setProductValueEach(input);
        } catch (Exception e) {
            assertEquals(e.getMessage(),productCountIsNone.getMessage());
        }
    }

    @Test
    public void interGalacticNumeralsToValue() {
        String input;
        double output,expected;

        List<InterGalacticNumeralObject> interGalacticNumerals = new ArrayList<>();
        interGalacticNumerals.add(new InterGalacticNumeralObject("glob","I"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("prok","V"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("pish","X"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("tegj","L"));
        interGalacticTranslator.setInterGalacticNumerals(interGalacticNumerals);

        input = "pish tegj glob glob";
        try {
            output = interGalacticTranslator.interGalacticNumeralsToValue(input);
            expected = 42;
            assertEquals(expected,output,0.1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void calculateProductValueFromInterGalacticCount() {
        ProductObject gold = new ProductObject("Gold",5);
        List<ProductObject> products = new ArrayList<>();
        products.add(gold);
        interGalacticTranslator.setProducts(products);

        List<InterGalacticNumeralObject> interGalacticNumerals = new ArrayList<>();
        interGalacticNumerals.add(new InterGalacticNumeralObject("glob","I"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("prok","V"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("pish","X"));
        interGalacticNumerals.add(new InterGalacticNumeralObject("tegj","L"));
        interGalacticTranslator.setInterGalacticNumerals(interGalacticNumerals);

        String input;
        double output,expected;

        input = "glob prok Gold";
        expected = 20;
        try {
            output = interGalacticTranslator.calculateProductValueFromInterGalacticCount(input);
            assertEquals(expected,output,0.1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = "prok glob glob glob Gold";
        expected = 40;
        try {
            output = interGalacticTranslator.calculateProductValueFromInterGalacticCount(input);
            assertEquals(expected,output,0.1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}