package com.kasidis.problems.one;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RouteManagerTest {

    List<String> possibleRoutes = new ArrayList<>();
    RouteManager routeManager = new RouteManager();

    @Before
    public void init() {
        possibleRoutes = new ArrayList<>(Arrays.asList("AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"));
        routeManager.setExistRoutes(possibleRoutes);
    }

    @Test
    public void findDistanceFromNodes() {

        String input, output, expected;

        input = "ABC";
        output = routeManager.findDistanceFromNodes(input);
        expected = "9";
        assertEquals(expected, output);

        input = "AD";
        output = routeManager.findDistanceFromNodes(input);
        expected = "5";
        assertEquals(expected, output);

        input = "ADC";
        output = routeManager.findDistanceFromNodes(input);
        expected = "13";
        assertEquals(expected, output);

        input = "AEBCD";
        output = routeManager.findDistanceFromNodes(input);
        expected = "22";
        assertEquals(expected, output);

        input = "AED";
        output = routeManager.findDistanceFromNodes(input);
        expected = "NO SUCH ROUTE";
        assertEquals(expected, output);
    }

    @Test
    public void findPossibleRoutes() {
        List<String> output;
        char inputBeginNode = 'C', inputEndNode = 'C';
        int inputMaxStopCount = 3;
        List <String> expected = new ArrayList<>(Arrays.asList("CDC", "CEBC"));
        output = routeManager.findPossibleRoutes(inputBeginNode, inputEndNode, inputMaxStopCount);
        assertEquals(expected, output);

        inputBeginNode = 'A';
        inputEndNode = 'C';
        inputMaxStopCount = 4;
        expected = new ArrayList<>(Arrays.asList("ABC", "ADC", "AEBC", "ABCDC", "ADCDC", "ADEBC"));
        output = routeManager.findPossibleRoutes(inputBeginNode, inputEndNode, inputMaxStopCount);
        assertEquals(expected, output);

        inputBeginNode = 'A';
        inputEndNode = 'C';
        inputMaxStopCount = 5;
        expected = new ArrayList<>(Arrays.asList("ABC", "ADC", "AEBC", "ABCDC", "ADCDC", "ADEBC","ABCEBC", "ADCEBC", "AEBCDC"));
        output = routeManager.findPossibleRoutes(inputBeginNode, inputEndNode, inputMaxStopCount);
        assertEquals(expected, output);

        inputBeginNode = 'B';
        inputEndNode = 'B';
        inputMaxStopCount = 5;
        expected = new ArrayList<>(Arrays.asList("BCEB", "BCDEB", "BCDCEB"));
        output = routeManager.findPossibleRoutes(inputBeginNode, inputEndNode, inputMaxStopCount);
        assertEquals(expected, output);
    }


    @Test
    public void routesCountUpToStopsCount() {
        List<String>inputList = new ArrayList<>(Arrays.asList("CDC", "CEBC"));
        int stopsCount = 3;
        int output,expected = 2;
        output = routeManager.routesCountUpToStopsCount(inputList,stopsCount);
        assertEquals(expected, output);
    }

    @Test
    public void routesCountEqualToStopsCount() {
        List<String>inputList = new ArrayList<>(Arrays.asList("ABC", "ADC", "AEBC", "ABCDC", "ADCDC", "ADEBC"));
        int stopsCount = 4;
        int output,expected = 3;
        output = routeManager.routesCountEqualToStopsCount(inputList,stopsCount);
        assertEquals(expected, output);
    }

    @Test
    public void shortestRoutePossible() {
        List<String>inputList = new ArrayList<>(Arrays.asList("ABC", "ADC", "AEBC", "ABCDC", "ADCDC", "ADEBC","ABCEBC", "ADCEBC", "AEBCDC"));
        int output, expected;
        expected = 9;
        output = routeManager.shortestRoutePossible(inputList);
        assertEquals(expected, output);

        inputList = new ArrayList<>(Arrays.asList("BCEB", "BCDEB", "BCDCEB"));
        expected = 9;
        output = routeManager.shortestRoutePossible(inputList);
        assertEquals(expected, output);
    }

    @Test
    public void possibleRoutesShorterThan() {
        List<String>inputList;
        List<String>outputList;
        List<String>expectedList;
        inputList = routeManager.findPossibleRoutes('C','C',30);
        outputList = routeManager.possibleRoutesShorterThan(inputList, 30);
        expectedList = new ArrayList<>(Arrays.asList("CDC", "CEBC", "CDEBC", "CDCEBC", "CEBCDC", "CEBCEBC", "CEBCEBCEBC"));
        assertEquals(expectedList, outputList);
    }
}