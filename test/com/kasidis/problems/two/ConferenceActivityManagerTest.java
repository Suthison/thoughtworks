package com.kasidis.problems.two;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ConferenceActivityManagerTest {

    ConferenceActivityManager conferenceActivityManager;

    @Before
    public void init() {
        conferenceActivityManager = new ConferenceActivityManager();
    }
    @Test
    public void formatActivityStringToObjectList() {
        List<String> inputList = new ArrayList<>();
        inputList.add("A 800min");
        inputList.add("B lightning");

        List<ConferenceActivityObject> outputList, expectedList = new ArrayList<>();
        expectedList.add(new ConferenceActivityObject("A", 800));
        expectedList.add(new ConferenceActivityObject("B", 15));

        try {
            conferenceActivityManager.formatActivityStringToObjectList(inputList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        outputList = conferenceActivityManager.getRawData();

        assertEquals(expectedList.get(0).getTime(), outputList.get(0).getTime());
        assertEquals(expectedList.get(1).getActivity(), outputList.get(1).getActivity());
    }

    @Test
    public void switchElementToRemoveTimeLeft() {
        int sessionFirstInputTimeLeft, sessionSecondInputTimeLeft;
        List<ConferenceActivityObject> sessionFirstInputList = new ArrayList<>();
        sessionFirstInputList.add(new ConferenceActivityObject("A", 15));
        sessionFirstInputList.add(new ConferenceActivityObject("B", 20));

        List<ConferenceActivityObject> sessionSecondInputList = new ArrayList<>();
        sessionSecondInputList.add(new ConferenceActivityObject("C", 30));
        sessionSecondInputList.add(new ConferenceActivityObject("D", 25));

        sessionFirstInputTimeLeft = 10;
        sessionSecondInputTimeLeft = 20;

        int sessionFirstExpectedTimeLeft, sessionSecondExpectedTimeLeft;
        List<ConferenceActivityObject> sessionFirstExpectedList = new ArrayList<>();
        sessionFirstExpectedList.add(new ConferenceActivityObject("D", 25));
        sessionFirstExpectedList.add(new ConferenceActivityObject("B", 20));

        List<ConferenceActivityObject> sessionSecondExpectedList = new ArrayList<>();
        sessionSecondExpectedList.add(new ConferenceActivityObject("C", 30));
        sessionSecondExpectedList.add(new ConferenceActivityObject("A", 15));

        sessionFirstExpectedTimeLeft = 0;
        sessionSecondExpectedTimeLeft = 30;

        SessionPlanObject sessionFirstInputObject = new SessionPlanObject(sessionFirstInputList, sessionFirstInputTimeLeft);
        SessionPlanObject sessionSecondInputObject = new SessionPlanObject(sessionSecondInputList, sessionSecondInputTimeLeft);

        conferenceActivityManager.switchElementToRemoveTimeLeft(sessionFirstInputObject, sessionSecondInputObject);
        SessionPlanObject sessionFirstOutputObject = sessionFirstInputObject;
        SessionPlanObject sessionSecondOutputObject = sessionSecondInputObject;

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getActivity()
                , sessionFirstExpectedList.get(0).getActivity());
        assertEquals(sessionFirstOutputObject.getActivitiesList().get(1).getActivity()
                , sessionFirstExpectedList.get(1).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getActivity()
                , sessionSecondExpectedList.get(0).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getActivity()
                , sessionSecondExpectedList.get(1).getActivity());

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getTime()
                , sessionFirstExpectedList.get(0).getTime());
        assertEquals(sessionFirstOutputObject.getActivitiesList().get(1).getTime()
                , sessionFirstExpectedList.get(1).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getTime()
                , sessionSecondExpectedList.get(0).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getTime()
                , sessionSecondExpectedList.get(1).getTime());

        assertEquals(sessionFirstOutputObject.getTimeLeft()
                , sessionFirstExpectedTimeLeft);
        assertEquals(sessionSecondOutputObject.getTimeLeft()
                , sessionSecondExpectedTimeLeft);
    }

    @Test
    public void switchElementToReduceTimeLeft() {
        int sessionFirstInputTimeLeft, sessionSecondInputTimeLeft;
        List<ConferenceActivityObject> sessionFirstInputList = new ArrayList<>();
        sessionFirstInputList.add(new ConferenceActivityObject("A", 15));
        sessionFirstInputList.add(new ConferenceActivityObject("B", 20));

        List<ConferenceActivityObject> sessionSecondInputList = new ArrayList<>();
        sessionSecondInputList.add(new ConferenceActivityObject("C", 30));
        sessionSecondInputList.add(new ConferenceActivityObject("D", 25));

        sessionFirstInputTimeLeft = 10;
        sessionSecondInputTimeLeft = 55;

        int sessionFirstExpectedTimeLeft, sessionSecondExpectedTimeLeft;
        List<ConferenceActivityObject> sessionFirstExpectedList = new ArrayList<>();
        sessionFirstExpectedList.add(new ConferenceActivityObject("A", 15));
        sessionFirstExpectedList.add(new ConferenceActivityObject("D", 25));

        List<ConferenceActivityObject> sessionSecondExpectedList = new ArrayList<>();
        sessionSecondExpectedList.add(new ConferenceActivityObject("C", 30));
        sessionSecondExpectedList.add(new ConferenceActivityObject("B", 20));

        sessionFirstExpectedTimeLeft = 5;
        sessionSecondExpectedTimeLeft = 60;

        SessionPlanObject sessionFirstInputObject = new SessionPlanObject(sessionFirstInputList, sessionFirstInputTimeLeft);
        SessionPlanObject sessionSecondInputObject = new SessionPlanObject(sessionSecondInputList, sessionSecondInputTimeLeft);

        conferenceActivityManager.switchElementToReduceTimeLeft(sessionFirstInputObject, sessionSecondInputObject);
        SessionPlanObject sessionFirstOutputObject = sessionFirstInputObject;
        SessionPlanObject sessionSecondOutputObject = sessionSecondInputObject;

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getActivity()
                , sessionFirstExpectedList.get(0).getActivity());
        assertEquals(sessionFirstOutputObject.getActivitiesList().get(1).getActivity()
                , sessionFirstExpectedList.get(1).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getActivity()
                , sessionSecondExpectedList.get(0).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getActivity()
                , sessionSecondExpectedList.get(1).getActivity());

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getTime()
                , sessionFirstExpectedList.get(0).getTime());
        assertEquals(sessionFirstOutputObject.getActivitiesList().get(1).getTime()
                , sessionFirstExpectedList.get(1).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getTime()
                , sessionSecondExpectedList.get(0).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getTime()
                , sessionSecondExpectedList.get(1).getTime());

        assertEquals(sessionFirstOutputObject.getTimeLeft()
                , sessionFirstExpectedTimeLeft);
        assertEquals(sessionSecondOutputObject.getTimeLeft()
                , sessionSecondExpectedTimeLeft);
    }

    @Test
    public void moveElementToReduceAnotherSessionTimeLeft() {
        int sessionFirstInputTimeLeft, sessionSecondInputTimeLeft;
        List<ConferenceActivityObject> sessionFirstInputList = new ArrayList<>();
        sessionFirstInputList.add(new ConferenceActivityObject("A", 15));
        sessionFirstInputList.add(new ConferenceActivityObject("B", 5));

        List<ConferenceActivityObject> sessionSecondInputList = new ArrayList<>();
        sessionSecondInputList.add(new ConferenceActivityObject("C", 30));
        sessionSecondInputList.add(new ConferenceActivityObject("D", 25));

        sessionFirstInputTimeLeft = 55;
        sessionSecondInputTimeLeft = 65;

        int sessionFirstExpectedTimeLeft, sessionSecondExpectedTimeLeft;
        List<ConferenceActivityObject> sessionFirstExpectedList = new ArrayList<>();
        sessionFirstExpectedList.add(new ConferenceActivityObject("A", 15));

        List<ConferenceActivityObject> sessionSecondExpectedList = new ArrayList<>();
        sessionSecondExpectedList.add(new ConferenceActivityObject("C", 30));
        sessionSecondExpectedList.add(new ConferenceActivityObject("D", 25));
        sessionSecondExpectedList.add(new ConferenceActivityObject("B", 5));

        sessionFirstExpectedTimeLeft = 60;
        sessionSecondExpectedTimeLeft = 60;

        SessionPlanObject sessionFirstInputObject = new SessionPlanObject(sessionFirstInputList, sessionFirstInputTimeLeft);
        SessionPlanObject sessionSecondInputObject = new SessionPlanObject(sessionSecondInputList, sessionSecondInputTimeLeft);
        conferenceActivityManager.moveElementToReduceAnotherSessionTimeLeft(sessionFirstInputObject, sessionSecondInputObject);
        SessionPlanObject sessionFirstOutputObject = sessionFirstInputObject;
        SessionPlanObject sessionSecondOutputObject = sessionSecondInputObject;

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getActivity()
                , sessionFirstExpectedList.get(0).getActivity());

        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getActivity()
                , sessionSecondExpectedList.get(0).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getActivity()
                , sessionSecondExpectedList.get(1).getActivity());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(2).getActivity()
                , sessionSecondExpectedList.get(2).getActivity());

        assertEquals(sessionFirstOutputObject.getActivitiesList().get(0).getTime()
                , sessionFirstExpectedList.get(0).getTime());

        assertEquals(sessionSecondOutputObject.getActivitiesList().get(0).getTime()
                , sessionSecondExpectedList.get(0).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(1).getTime()
                , sessionSecondExpectedList.get(1).getTime());
        assertEquals(sessionSecondOutputObject.getActivitiesList().get(2).getTime()
                , sessionSecondExpectedList.get(2).getTime());

        assertEquals(sessionFirstOutputObject.getTimeLeft()
                , sessionFirstExpectedTimeLeft);
        assertEquals(sessionSecondOutputObject.getTimeLeft()
                , sessionSecondExpectedTimeLeft);
    }
}