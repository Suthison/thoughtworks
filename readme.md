## Overview

Created by Kasidis Suthison

Programming language : JAVA (Java SE Development Kit 12)

Test library : JUNIT(4.12)

## Build & Run

Every problems take input as text file

If you would like to change input please kindly change specific file

        problem_[problem number]_001.txt

I have prepared two ways to execute this application

in case one might failed

#### Build & Run with docker

Require Docker latest version Please check at [url](https://docs.docker.com/install/)

This dockerfile use openjdk:12-jdk as base image
to build docker image make sure you are in the same path with Dockerfile
then execute this command

        $docker build --rm --no-cache -t [imagename] .

#### Build & Run with IntelliJ IDE

you can import this project with IntelliJ IDE

Make sure you set work directory in configuration as

        [your path]/thoughtworks/src

## Application description

#### Problem one: Trains

Take all possible routes via input
then loop through the list to calculate problems

#### Problem two: Conference Track Management

The idea is to loop through input and put the element in each session
where can fit in the session
Then switch elements between session to minimize time left
(Session morning must have 0 time left)

#### Problem three: Merchant's Guide to the Galaxy

Create logic to calculate roman numerals first
The idea of roman numerals calculation is
roman numerals is always sort from max to min

Then create method to calculate between intergalactic and roman numerials

