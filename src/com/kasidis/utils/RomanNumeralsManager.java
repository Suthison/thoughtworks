package com.kasidis.utils;

import com.kasidis.CustomException;

import javax.swing.text.MutableAttributeSet;
import java.util.ArrayList;
import java.util.List;

public class RomanNumeralsManager {

    private int[] romanAlphabetValues = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    private String[] romanAlphabetCharacters = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

    public int[] getRomanAlphabetValues() {
        return romanAlphabetValues;
    }

    public void setRomanAlphabetValues(int[] romanAlphabetValues) {
        this.romanAlphabetValues = romanAlphabetValues;
    }

    public String[] getRomanAlphabetCharacters() {
        return romanAlphabetCharacters;
    }

    public void setRomanAlphabetCharacters(String[] romanAlphabetCharacters) {
        this.romanAlphabetCharacters = romanAlphabetCharacters;
    }

    public int romanNumeralsToValue(String romanNumerals) throws Exception {
        CustomException customException = new CustomException();
        int result = 0;

        for (int i = 0; i < romanAlphabetValues.length; i++) {
            while (romanNumerals.indexOf(romanAlphabetCharacters[i]) == 0) {
                result += romanAlphabetValues[i];
                romanNumerals = romanNumerals.substring(romanAlphabetCharacters[i].length());
            }
        }
        //If there is string left in romanNumerals
        if (!romanNumerals.equals("")) {
            throw customException.romanNumeralsContainUnexpectedValue;
        }
        return result;
    }

    public String valueToRomanNumerals(int value) {
        String valueString = String.valueOf(value);
        String resultRomanNumerals = "";

        for (int i = 0; i < valueString.length() ; i++) {
            int digitNumber = Character.getNumericValue(valueString.charAt(i));
            if (digitNumber == 0) {
                //do nothing
            } else {
                //Digit value at index i
                int digitValue = (int) ((digitNumber) * Math.pow(10, valueString.length()-1-i)); //1944 -> 1000 900 40 4
                resultRomanNumerals = resultRomanNumerals + digitValueToRomanNumeral(digitValue);
            }
        }
        return resultRomanNumerals;
    }

    public String digitValueToRomanNumeral(int value) {
        String romanNemeral = "";

        int tmp = value;
        for (int i = 0; i < romanAlphabetValues.length; i++) {
            while (romanAlphabetValues[i] <= tmp && tmp - romanAlphabetValues[i] >= 0) {
                romanNemeral = romanNemeral + romanAlphabetCharacters[i];
                tmp = tmp - romanAlphabetValues[i];
            }
            if (tmp == 0) {
                return romanNemeral;
            }
        }
        return romanNemeral;
    }

}
