package com.kasidis.utils;

import java.util.List;

public class ArrayListManager {
    public String findMatchInArrayList(List<String> inputList, String searchElement){

        for(String str: inputList) {
            if(str.trim().contains(searchElement))
                return str;
        }
        return "";
    }
}
