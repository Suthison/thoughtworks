package com.kasidis.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileManager {

    public List<String> readFileByLine(String filePath){
        File file = new File(filePath);
        String line;
        List<String> resultList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while ((line = bufferedReader.readLine()) != null){
                resultList.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultList;
    }
    public List<String> readFileByCommaAndWhiteSpace(String filePath){
        List<String> resultList;
        List<String> tempResultList;

        tempResultList = readFileByLine("input/problem_one_001.txt");
        String tempOutputString = tempResultList.get(0);
        String [] tempOutputStringArray = tempOutputString.split(",\\s*");
        resultList = new ArrayList<>(Arrays.asList(tempOutputStringArray));
        return resultList;
    }

}
