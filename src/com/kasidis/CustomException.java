package com.kasidis;

public class CustomException {
    public static Exception romanNumeralsContainUnexpectedValue = new Exception("Roman numerals contain unexpeted value");
    public static Exception romanNumeralsNotExist = new Exception("Input roman numeral not exist");
    public static Exception interGalacticNumeralNotMatch = new Exception("InterGalactic numerals not matched");
    public static Exception productCountIsNone = new Exception("Product count is none");
    public static Exception productNotfoundException = new Exception("Product count is none");

    public static Exception totalTimeException = new Exception("Total activities time must not more than available time or less than available time for 120 minutes");

}
