package com.kasidis.problems.three;

import com.kasidis.CustomException;
import com.kasidis.utils.RomanNumeralsManager;

import java.util.ArrayList;
import java.util.List;

import static com.kasidis.CustomException.*;

public class InterGalacticTranslator {

    private List<InterGalacticNumeralObject> interGalacticNumerals = new ArrayList<>();
    private List<ProductObject> products = new ArrayList<>();

    public List<InterGalacticNumeralObject> getInterGalacticNumerals() {
        return interGalacticNumerals;
    }

    public void setInterGalacticNumerals(List<InterGalacticNumeralObject> interGalacticNumerals) {
        this.interGalacticNumerals = interGalacticNumerals;
    }

    public List<ProductObject> getProducts() {
        return products;
    }

    public void setProducts(List<ProductObject> products) {
        this.products = products;
    }

    public String messageTranslate(String inputMessage) {

        String replyMessage = "I have no idea what you are talking about";
        String[] words = inputMessage.split("\\s+");

        if (words.length == 3 && words[1].equals("is")) {
            //SetInterGalacticToRomanNumerals
            InterGalacticNumeralObject interGalacticNumeral;
            try {
                interGalacticNumeral = setInterGalacticToRomanNumerals(words[0], words[2]);
                interGalacticNumerals.add(interGalacticNumeral);
                return "";
            } catch (Exception e) {
                return "I have no idea what you are talking about";
            }
        }
        else if (words[words.length - 1].equals("Credits") && words[words.length - 3].equals("is")) {
            //SetProductValueEach
            try {
                setProductValueEach(inputMessage);
                return "";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (inputMessage.indexOf("how much is") == 0) {
            //ConvertInterGalacticToValue
            String [] interGalacticNumerals = inputMessage.split("\\s+is\\s+");
            String interGalacticNumeralsStringWithQuestionMark = interGalacticNumerals[1];
            String interGalacticNumeralsString = interGalacticNumeralsStringWithQuestionMark.substring(0,interGalacticNumeralsStringWithQuestionMark.indexOf(" ?"));
            try {
                int value = (int) interGalacticNumeralsToValue(interGalacticNumeralsString);
                return String.format("%s is %d",interGalacticNumeralsString, value);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (inputMessage.indexOf("how many Credits is") == 0) {
            //CalculateProductValue
            String[] interGalacticProductCountWords = inputMessage.split("\\s+is\\s+");
            String interGalacticProductCountStringWithQuestionMark = interGalacticProductCountWords[1];
            String interGalacticProductCountString = interGalacticProductCountStringWithQuestionMark.substring(0, interGalacticProductCountStringWithQuestionMark.indexOf(" ?"));

            try {
                int value = (int) calculateProductValueFromInterGalacticCount(interGalacticProductCountString);
                return String.format("%s is %d credits", interGalacticProductCountString, value);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return replyMessage;
    }

    public double calculateProductValueFromInterGalacticCount(String interGalacticProductCountString) throws Exception {

        double totalValue;

        String [] words = interGalacticProductCountString.split("\\s+");
        String productName = words[words.length-1];

        double productValueEach = 0;
        for(int i=0;i<products.size();i++){
            if(productName.equals(products.get(i).getProductName())){
                productValueEach = products.get(i).getProductValue();
            }
        }
        if(productValueEach == 0){
            throw productNotfoundException;
        }

        String interGalacticCount = interGalacticProductCountString.substring(0,interGalacticProductCountString.lastIndexOf(" "));

        int count = (int) interGalacticNumeralsToValue(interGalacticCount);
        totalValue = productValueEach * count;

        return totalValue;

    }

    public void setProductValueEach(String inputMessage) throws Exception {
        //Split with is
        String[] splitByIsWords = inputMessage.split("\\s+is\\s+");
        String productCountString = splitByIsWords[0];
        String productTotalValueString = splitByIsWords[1];

        String[] productCountStringWords = productCountString.split("\\s+");
        int productCount;

        //productCountStringWords except last word
        String romanNumerals = "";
        for (int i = 0; i < productCountStringWords.length - 1; i++) {
            try {
                romanNumerals = romanNumerals + interGalacticNumeralToRomanNumeral(productCountStringWords[i]);
            } catch (Exception e) {
                throw e;
            }
        }
        RomanNumeralsManager romanNumeralsManager = new RomanNumeralsManager();
        try {
            productCount = romanNumeralsManager.romanNumeralsToValue(romanNumerals);

            if(productCount == 0){
                throw productCountIsNone;
            }

            String productName = productCountStringWords[productCountStringWords.length - 1];

            //Product total values
            String[] productTotalValueStringWords = productTotalValueString.split("\\s+");
            double productTotalValue = Integer.parseInt(productTotalValueStringWords[0]);

            double productValueEach = productTotalValue / productCount;

            ProductObject productObject = new ProductObject(productName, productValueEach);

            boolean productWasAdded = false;
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).getProductName().equals(productName)) {
                    productWasAdded = true;
                    products.set(i, productObject);
                    break;
                }
            }
            if (!productWasAdded) {
                products.add(productObject);
            }


        } catch (Exception e) {
            throw e;
        }
    }

    public InterGalacticNumeralObject setInterGalacticToRomanNumerals(String interGalacticNumeral, String romanNumeral) throws Exception {

        //Check if roman numeral is exist
        boolean isInputRomanNumeralExist = false;
        RomanNumeralsManager romanNumeralsManager = new RomanNumeralsManager();
        String[] availableRomanNumerals = romanNumeralsManager.getRomanAlphabetCharacters();
        for (int i = 0; i < availableRomanNumerals.length; i++) {
            if (availableRomanNumerals[i].equals(romanNumeral)) {
                isInputRomanNumeralExist = true;
                break;
            }
        }
        if (!isInputRomanNumeralExist) {
            CustomException customException = new CustomException();
            throw customException.romanNumeralsNotExist;
        }

        //If exist
        InterGalacticNumeralObject interGalacticNumeralObject = new InterGalacticNumeralObject(interGalacticNumeral, romanNumeral);
        return interGalacticNumeralObject;
    }

    public double interGalacticNumeralsToValue(String interGalacticNumeralsString) throws Exception {
        double value;
        String romanNumerals = "";
        String[] interGalacticNumerals = interGalacticNumeralsString.split("\\s+");
        for (int i = 0; i < interGalacticNumerals.length; i++) {
            romanNumerals = romanNumerals + interGalacticNumeralToRomanNumeral(interGalacticNumerals[i]);
        }

        RomanNumeralsManager romanNumeralsManager = new RomanNumeralsManager();
        try {
            value = romanNumeralsManager.romanNumeralsToValue(romanNumerals);
        } catch (Exception e) {
            throw e;
        }

        return value;
    }

    public String interGalacticNumeralToRomanNumeral(String interGalacticNumeral) throws Exception {

        String romanNumeral = "";

        for (int j = 0; j < interGalacticNumerals.size(); j++) {
            //match found!
            if (interGalacticNumeral.equals(interGalacticNumerals.get(j).getInterGalacticNumeral())) {
                romanNumeral = interGalacticNumerals.get(j).getRomanNumeral();
                break;
            }
        }
        if (romanNumeral.equals("")) {
            throw interGalacticNumeralNotMatch;
        }
        return romanNumeral;
    }


}
