package com.kasidis.problems.three;

import com.kasidis.utils.FileManager;

import java.util.List;

public class ProblemThreeMain {
    public static void main(String[] args) {

        List<String> inputMessageList;
        FileManager fileManager = new FileManager();

        inputMessageList = fileManager.readFileByLine("input/problem_three_001.txt");

        InterGalacticTranslator interGalacticTranslator = new InterGalacticTranslator();

        for(int i=0;i<inputMessageList.size();i++){
            String outputMessage = interGalacticTranslator.messageTranslate(inputMessageList.get(i));
            if(!outputMessage.equals("")){
                System.out.println(outputMessage);
            }
        }
    }
}
