package com.kasidis.problems.three;

public class ProductObject {

    private String productName;
    private double productValue;

    public ProductObject(String productName, double productValue) {
        this.productName = productName;
        this.productValue = productValue;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductValue() {
        return productValue;
    }

    public void setProductValue(double productValue) {
        this.productValue = productValue;
    }
}
