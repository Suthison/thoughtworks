package com.kasidis.problems.three;

public class InterGalacticNumeralObject {

    String interGalacticNumeral;
    String romanNumeral;

    public InterGalacticNumeralObject(String interGalacticNumerals, String romanNumerals) {
        this.interGalacticNumeral = interGalacticNumerals;
        this.romanNumeral = romanNumerals;
    }

    public String getInterGalacticNumeral() {
        return interGalacticNumeral;
    }

    public void setInterGalacticNumeral(String interGalacticNumerals) {
        this.interGalacticNumeral = interGalacticNumerals;
    }

    public String getRomanNumeral() {
        return romanNumeral;
    }

    public void setRomanNumeral(String romanNumerals) {
        this.romanNumeral = romanNumerals;
    }

}
