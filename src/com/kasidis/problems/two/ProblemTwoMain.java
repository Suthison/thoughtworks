package com.kasidis.problems.two;

import com.kasidis.utils.DateManager;
import com.kasidis.utils.FileManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProblemTwoMain {
    public static void main(String[] args) {
        List<String> activityStringList;
        FileManager fileManager = new FileManager();

        activityStringList = fileManager.readFileByLine("input/problem_two_001.txt");

        ConferenceActivityManager conferenceActivityManager = new ConferenceActivityManager();
        try {
            conferenceActivityManager.formatActivityStringToObjectList(activityStringList);
            conferenceActivityManager.generateSessionActivities();
            List<ConferenceActivityObject> sessionOne = new ArrayList<>(conferenceActivityManager.getSessionOne().getActivitiesList());
            List<ConferenceActivityObject> sessionTwo = new ArrayList<>(conferenceActivityManager.getSessionTwo().getActivitiesList());
            List<ConferenceActivityObject> sessionThree = new ArrayList<>(conferenceActivityManager.getSessionThree().getActivitiesList());
            List<ConferenceActivityObject> sessionFour = new ArrayList<>(conferenceActivityManager.getSessionFour().getActivitiesList());

            System.out.println("Track 1");
            printSession(sessionOne,true);
            printSession(sessionTwo, false);

            System.out.println("Track 2");
            printSession(sessionThree, true);
            printSession(sessionFour, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printSession(List<ConferenceActivityObject> sessionList, boolean isMorning) {
        DateFormat dateFormat = new SimpleDateFormat("hh:mma");
        DateManager dateManager = new DateManager();
        String currentTimeString;
        if(isMorning == true){
            currentTimeString = ("09:00AM");
        }else{
            currentTimeString = ("01:00PM");
        }
        Date currentTimeDate = null;
        try {
            currentTimeDate = dateFormat.parse(currentTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < sessionList.size(); i++) {
            String timeSpent;
            String activity;
            if(sessionList.get(i).getTime() == 15){
                timeSpent = "lightning";
            }else{
                timeSpent = sessionList.get(i).getTime() + "min";
            }
            activity = sessionList.get(i).getActivity();

            System.out.println(String.format("%s %s %s",currentTimeString,activity,timeSpent));
            currentTimeDate = dateManager.addMinutesToJavaUtilDate(currentTimeDate, sessionList.get(i).getTime());
            currentTimeString = dateFormat.format(currentTimeDate);
        }
        if(isMorning == true){
            System.out.println(String.format("%s Lunch",currentTimeString));
        }else{
            System.out.println(String.format("%s Networking Event",currentTimeString));
        }
    }
}
