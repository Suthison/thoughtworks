package com.kasidis.problems.two;

public class ConferenceActivityObject {
    private String activity;
    private int time;

    public ConferenceActivityObject(String activity, int time) {
        this.activity = activity;
        this.time = time;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
