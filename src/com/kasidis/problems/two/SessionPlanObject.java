package com.kasidis.problems.two;

import java.util.List;

public class SessionPlanObject {
    public SessionPlanObject(List<ConferenceActivityObject> activitiesList, int timeLeft) {
        this.activitiesList = activitiesList;
        this.timeLeft = timeLeft;
    }

    private List<ConferenceActivityObject> activitiesList;
    private int timeLeft;

    public List<ConferenceActivityObject> getActivitiesList() {
        return activitiesList;
    }

    public void setActivitiesList(List<ConferenceActivityObject> activitiesList) {
        this.activitiesList = activitiesList;
    }

    public int getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(int timeLeft) {
        this.timeLeft = timeLeft;
    }



}
