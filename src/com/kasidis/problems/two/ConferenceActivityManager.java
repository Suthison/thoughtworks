package com.kasidis.problems.two;

import java.util.ArrayList;
import java.util.List;

import static com.kasidis.CustomException.totalTimeException;

public class ConferenceActivityManager {
    SessionPlanObject sessionOne = new SessionPlanObject(new ArrayList<>(), 0);
    SessionPlanObject sessionTwo = new SessionPlanObject(new ArrayList<>(), 0);
    SessionPlanObject sessionThree = new SessionPlanObject(new ArrayList<>(), 0);
    SessionPlanObject sessionFour = new SessionPlanObject(new ArrayList<>(), 0);
    List<ConferenceActivityObject> RawData = new ArrayList<>();

    public SessionPlanObject getSessionOne() {
        return sessionOne;
    }

    public void setSessionOne(SessionPlanObject sessionOne) {
        this.sessionOne = sessionOne;
    }

    public SessionPlanObject getSessionTwo() {
        return sessionTwo;
    }

    public void setSessionTwo(SessionPlanObject sessionTwo) {
        this.sessionTwo = sessionTwo;
    }

    public SessionPlanObject getSessionThree() {
        return sessionThree;
    }

    public void setSessionThree(SessionPlanObject sessionThree) {
        this.sessionThree = sessionThree;
    }

    public SessionPlanObject getSessionFour() {
        return sessionFour;
    }

    public void setSessionFour(SessionPlanObject sessionFour) {
        this.sessionFour = sessionFour;
    }

    public List<ConferenceActivityObject> getRawData() {
        return RawData;
    }

    public void setRawData(List<ConferenceActivityObject> rawData) {
        RawData = rawData;
    }

    public void formatActivityStringToObjectList(List<String> activityStringList) throws Exception {
        List<ConferenceActivityObject> objectList = new ArrayList<>();

        int totalActivitiesTime = 0;
        for (int i = 0; i < activityStringList.size(); i++) {
            String tempString = activityStringList.get(i);
            int index = tempString.lastIndexOf(" ");
            String time = tempString.substring(index + 1);
            String activity = tempString.substring(0, index);
            if (!time.equals("lightning")) {
                time = time.replaceAll("\\D+", "");
            } else {
                time = "15";
            }
            ConferenceActivityObject object = new ConferenceActivityObject(activity,Integer.parseInt(time));
            objectList.add(object);
            totalActivitiesTime = totalActivitiesTime + Integer.parseInt(time);
        }
        if(totalActivitiesTime>840 || totalActivitiesTime<720){
            throw totalTimeException;
        }
        this.setRawData(objectList);
    }

    public void generateSessionActivities() {
        //Initial time left
        this.setSessionOne(new SessionPlanObject(new ArrayList<>(), 180));
        this.setSessionTwo(new SessionPlanObject(new ArrayList<>(), 240));
        this.setSessionThree(new SessionPlanObject(new ArrayList<>(), 180));
        this.setSessionFour(new SessionPlanObject(new ArrayList<>(), 240));

        for (int i = 0; i < this.RawData.size(); i++) {
            int currentTime = this.RawData.get(i).getTime();
            //Put in the first session
            if (currentTime <= this.sessionOne.getTimeLeft()) {
                List<ConferenceActivityObject> tmp = new ArrayList<>();
                tmp = this.sessionOne.getActivitiesList();
                tmp.add(this.RawData.get(i));
                this.sessionOne.setActivitiesList(tmp);
                this.sessionOne.setTimeLeft(this.sessionOne.getTimeLeft() - currentTime);
            }
            //If cannot put in first session put in the second session
            else if (currentTime <= this.sessionTwo.getTimeLeft()) {
                List<ConferenceActivityObject> tmp = new ArrayList<>();
                tmp = this.sessionTwo.getActivitiesList();
                tmp.add(this.RawData.get(i));
                this.sessionTwo.setActivitiesList(tmp);
                this.sessionTwo.setTimeLeft(this.sessionTwo.getTimeLeft() - currentTime);
            }
            //If cannot put in first and second session put in the third session
            else if (currentTime <= this.sessionThree.getTimeLeft()) {
                List<ConferenceActivityObject> tmp = new ArrayList<>();
                tmp = this.sessionThree.getActivitiesList();
                tmp.add(this.RawData.get(i));
                this.sessionThree.setActivitiesList(tmp);
                this.sessionThree.setTimeLeft(this.sessionThree.getTimeLeft() - currentTime);
            }
            //If cannot put in first and second and third session put in the fourth session
            else if (currentTime <= this.sessionFour.getTimeLeft()) {
                List<ConferenceActivityObject> tmp = new ArrayList<>();
                tmp = this.sessionFour.getActivitiesList();
                tmp.add(this.RawData.get(i));
                this.sessionFour.setActivitiesList(tmp);
                this.sessionFour.setTimeLeft(this.sessionFour.getTimeLeft() - currentTime);
            }
        }
        //Prioritize morning session first
        if (sessionOne.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionOne, sessionTwo);
        }
        if (sessionOne.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionOne, sessionThree);
        }
        if (sessionOne.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionOne, sessionFour);
        }
        if (sessionThree.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionThree, sessionTwo);
        }
        if (sessionThree.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionThree, sessionFour);
        }
        if (sessionTwo.getTimeLeft() != 0) {
            switchElementToRemoveTimeLeft(sessionTwo, sessionFour);
        }

        //If Session four time left > 60
        //find activity from session two which is <=60
        //remove that activity and add it to last day
        if (sessionFour.getTimeLeft() > 60) {
            moveElementToReduceAnotherSessionTimeLeft(sessionTwo,sessionFour);
        }
        //If Session four time left still > 60
        //find element to switch with session two
        if (sessionFour.getTimeLeft() > 60) {
            switchElementToReduceTimeLeft(sessionFour,sessionTwo);
        }

    }

    public void switchElementToRemoveTimeLeft(SessionPlanObject sessionFirst, SessionPlanObject sessionSecond) {
        for (int i = 0; i < sessionFirst.getActivitiesList().size(); i++) {
            for (int j = 0; j < sessionSecond.getActivitiesList().size(); j++) {
                if (sessionSecond.getActivitiesList().get(j).getTime() - sessionFirst.getActivitiesList().get(i).getTime() == sessionFirst.getTimeLeft()) {
                    //switch
                    ConferenceActivityObject tmpObjectFirst;
                    ConferenceActivityObject tmpObjectSecond;

                    tmpObjectFirst = sessionFirst.getActivitiesList().get(i);
                    tmpObjectSecond = sessionSecond.getActivitiesList().get(j);

                    List<ConferenceActivityObject> tmpListFirst = new ArrayList<>(sessionFirst.getActivitiesList());
                    List<ConferenceActivityObject> tmpListSecond = new ArrayList<>(sessionSecond.getActivitiesList());

                    tmpListFirst.set(i, tmpObjectSecond);
                    tmpListSecond.set(j, tmpObjectFirst);

                    int tmpTimeFirst = sessionFirst.getActivitiesList().get(i).getTime();
                    int tmpTimeSecond = sessionSecond.getActivitiesList().get(j).getTime();
                    int tmpTime = tmpTimeSecond - tmpTimeFirst;
                    sessionFirst.setTimeLeft(sessionFirst.getTimeLeft() - tmpTime);
                    sessionSecond.setTimeLeft(sessionSecond.getTimeLeft() + tmpTime);

                    sessionFirst.setActivitiesList(tmpListFirst);
                    sessionSecond.setActivitiesList(tmpListSecond);

                    if(sessionFirst.getTimeLeft() == 0){
                        break;
                    }
                }
            }
        }
    }
    public void switchElementToReduceTimeLeft(SessionPlanObject sessionFirst, SessionPlanObject sessionSecond) {
        for (int i = 0; i < sessionFirst.getActivitiesList().size(); i++) {
            for (int j = 0; j < sessionSecond.getActivitiesList().size(); j++) {
                if (sessionSecond.getActivitiesList().get(j).getTime() - sessionFirst.getActivitiesList().get(i).getTime() <= 60-sessionSecond.getTimeLeft()
                        &&sessionSecond.getTimeLeft() < 60) {
                    //switch
                    ConferenceActivityObject tmpObjectFirst;
                    ConferenceActivityObject tmpObjectSecond;

                    tmpObjectFirst = sessionFirst.getActivitiesList().get(i);
                    tmpObjectSecond = sessionSecond.getActivitiesList().get(j);

                    List<ConferenceActivityObject> tmpListFirst = new ArrayList<>(sessionFirst.getActivitiesList());
                    List<ConferenceActivityObject> tmpListSecond = new ArrayList<>(sessionSecond.getActivitiesList());

                    tmpListFirst.set(i, tmpObjectSecond);
                    tmpListSecond.set(j, tmpObjectFirst);

                    int tmpTimeFirst = sessionFirst.getActivitiesList().get(i).getTime();
                    int tmpTimeSecond = sessionSecond.getActivitiesList().get(j).getTime();
                    int tmpTime = tmpTimeSecond - tmpTimeFirst;
                    sessionFirst.setTimeLeft(sessionFirst.getTimeLeft() - tmpTime);
                    sessionSecond.setTimeLeft(sessionSecond.getTimeLeft() + tmpTime);

                    sessionFirst.setActivitiesList(tmpListFirst);
                    sessionSecond.setActivitiesList(tmpListSecond);

                    if(sessionFirst.getTimeLeft() <= 60){
                        break;
                    }
                }
            }
        }
    }
    public void moveElementToReduceAnotherSessionTimeLeft(SessionPlanObject sessionFirst, SessionPlanObject sessionSecond){
        for (int i = 0; i < sessionFirst.getActivitiesList().size(); i++) {
            if (sessionFirst.getActivitiesList().get(i).getTime() + sessionFirst.getTimeLeft() <= 60 && sessionSecond.getTimeLeft() > 60) {
                List<ConferenceActivityObject> tmpFirst;
                List<ConferenceActivityObject> tmpSecond;
                tmpFirst = new ArrayList<>(sessionFirst.getActivitiesList());
                tmpSecond = new ArrayList<>(sessionSecond.getActivitiesList());
                tmpFirst.remove(i);
                tmpSecond.add(sessionFirst.getActivitiesList().get(i));

                sessionFirst.setTimeLeft(sessionFirst.getTimeLeft() + sessionFirst.getActivitiesList().get(i).getTime());
                sessionSecond.setTimeLeft(sessionSecond.getTimeLeft() - sessionFirst.getActivitiesList().get(i).getTime());

                sessionFirst.setActivitiesList(tmpFirst);
                sessionSecond.setActivitiesList(tmpSecond);
            }
        }
    }


}
