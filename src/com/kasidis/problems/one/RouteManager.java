package com.kasidis.problems.one;

import com.kasidis.utils.ArrayListManager;

import java.util.ArrayList;
import java.util.List;

public class RouteManager {

    private List<String> existRoutes = new ArrayList<>();

    public List<String> getExistRoutes() {
        return existRoutes;
    }

    public void setExistRoutes(List<String> possilbeRoutes) {
        this.existRoutes = possilbeRoutes;
    }

    public String findDistanceFromNodes(String inputNodes) {
        String distance = "NO SUCH ROUTE";
        int totalDistance = 0;
        for (int i = 0; i < inputNodes.length() - 1; i++) {
            String route = Character.toString(inputNodes.charAt(i)) + inputNodes.charAt(i + 1);
            ArrayListManager arrayListManager = new ArrayListManager();
            String matchRouteWithDistance = arrayListManager.findMatchInArrayList(this.existRoutes, route);
            if (!matchRouteWithDistance.equals("")) {
                char routeDistanceChar = matchRouteWithDistance.charAt(2);
                int routeDistance = Character.getNumericValue(routeDistanceChar);
                totalDistance = totalDistance + routeDistance;
            } else {
                return distance;
            }
        }
        distance = String.valueOf(totalDistance);
        return distance;
    }

    public int routesCountUpToStopsCount(List<String> possibleRoutes, int stopsCount) {
        int count = 0;
        for (int i = 0; i < possibleRoutes.size(); i++) {
            if (possibleRoutes.get(i).length() - 1 <= stopsCount) {
                count++;
            }
        }
        return count;
    }

    public int routesCountEqualToStopsCount(List<String> possibleRoutes, int stopsCount) {
        int count = 0;
        for (int i = 0; i < possibleRoutes.size(); i++) {
            if (possibleRoutes.get(i).length() - 1 == stopsCount) {
                count++;
            }
        }
        return count;
    }

    public List<String> findPossibleRoutes(char beginNode, char endNode, int maxStopCount) {

        List<String> resultPossibleRoutes = new ArrayList<>();

        List<String> possibleRoutesTwoNodes = new ArrayList<>();

        for (int i = 0; i < existRoutes.size(); i++) {
            if (existRoutes.get(i).charAt(0) == beginNode) {
                possibleRoutesTwoNodes.add(existRoutes.get(i));
            }
        }
        List<String> possibleRoutesTemp;
        possibleRoutesTemp = possibleRoutesTwoNodes;
        List<String> possibleRoutesTempResult = new ArrayList<>();

        for (int nodeCount = 0; nodeCount < maxStopCount - 1; nodeCount++) {
            for (int i = 0; i < possibleRoutesTemp.size(); i++) {
                for (int j = 0; j < existRoutes.size(); j++) {
                    //If last string character equal first character of exist route
                    if (possibleRoutesTemp.get(i).charAt(nodeCount + 1) == existRoutes.get(j).charAt(0)) {
                        StringBuilder tempRoutes = new StringBuilder();
                        for (int x = -2; x < nodeCount; x++) {
                            tempRoutes.append(possibleRoutesTemp.get(i).charAt(x + 2));
                        }
                        tempRoutes.append(existRoutes.get(j).charAt(1));
                        possibleRoutesTempResult.add(tempRoutes.toString());

                        if (tempRoutes.charAt(nodeCount + 2) == endNode) {
                            resultPossibleRoutes.add(tempRoutes.toString());
                        }
                    }
                }
            }
            possibleRoutesTemp = new ArrayList<>(possibleRoutesTempResult);
            possibleRoutesTempResult.clear();
        }

        return resultPossibleRoutes;
    }

    public int shortestRoutePossible(List<String> routes) {
        int distance = 0;
        List<String> routesDistance = new ArrayList<>();
        for (int i = 0; i < routes.size(); i++) {
            int tempDistance = Integer.parseInt(findDistanceFromNodes(routes.get(i)));
            routesDistance.add(String.valueOf(tempDistance));
        }
        for (int i = 0; i < routesDistance.size(); i++) {
            if (i == 0) {
                distance = Integer.parseInt(routesDistance.get(i));
            }
            if (Integer.parseInt(routesDistance.get(i)) < distance) {
                distance = Integer.parseInt(routesDistance.get(i));
            }
        }
        return distance;
    }

    public List<String> possibleRoutesShorterThan(List<String> routes, int maxDistance) {
        List<String> routesDistance = new ArrayList<>();
        List<String> resultRoutes = new ArrayList<>();
        for (int i = 0; i < routes.size(); i++) {
            int tempDistance = Integer.parseInt(findDistanceFromNodes(routes.get(i)));
            routesDistance.add(String.valueOf(tempDistance));
        }
        for (int i = 0; i < routesDistance.size(); i++) {
            if (Integer.parseInt(routesDistance.get(i)) < maxDistance) {
                resultRoutes.add(routes.get(i));
            }
        }
        return resultRoutes;
    }
}
