package com.kasidis.problems.one;

import com.kasidis.utils.FileManager;

import java.util.List;

public class ProblemOneMain {
    public static void main(String[] args) {
        List<String> possibleRoutes;
        RouteManager routeManager = new RouteManager();

        FileManager fileManager = new FileManager();

        possibleRoutes = fileManager.readFileByCommaAndWhiteSpace("input/problem_one_001.txt");
        routeManager.setExistRoutes(possibleRoutes);

        String resultNumberOne = routeManager.findDistanceFromNodes("ABC");
        String resultNumberTwo = routeManager.findDistanceFromNodes("AD");
        String resultNumberThree = routeManager.findDistanceFromNodes("ADC");
        String resultNumberFour = routeManager.findDistanceFromNodes("AEBCD");
        String resultNumberFive = routeManager.findDistanceFromNodes("AED");
        String resultNumberSix = String.valueOf(routeManager
                .routesCountUpToStopsCount(routeManager
                        .findPossibleRoutes('C', 'C', 3), 3));
        String resultNumberSeven = String.valueOf(routeManager
                .routesCountEqualToStopsCount(routeManager
                        .findPossibleRoutes('A', 'C', 4), 4));
        String resultNumberEight = String.valueOf(routeManager
                .shortestRoutePossible(routeManager
                        .findPossibleRoutes('A', 'C', 10)));
        String resultNumberNine = String.valueOf(routeManager
                .shortestRoutePossible(routeManager
                        .findPossibleRoutes('B', 'B', 10)));
        String resultNumberTen = String.valueOf(routeManager
                .possibleRoutesShorterThan(routeManager
                        .findPossibleRoutes('C', 'C', 30), 30).size());

        printOutput(1, resultNumberOne);
        printOutput(2, resultNumberTwo);
        printOutput(3, resultNumberThree);
        printOutput(4, resultNumberFour);
        printOutput(5, resultNumberFive);
        printOutput(6, resultNumberSix);
        printOutput(7, resultNumberSeven);
        printOutput(8, resultNumberEight);
        printOutput(9, resultNumberNine);
        printOutput(10, resultNumberTen);
    }

    public static void printOutput(int count, String result) {
        System.out.println(String.format("Output #%d: %s", count, result));
    }
}
