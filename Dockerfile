FROM openjdk:12-jdk
RUN mkdir -p /thoughtworks
COPY src /thoughtworks/
WORKDIR /thoughtworks

RUN javac -d . com/kasidis/*.java com/kasidis/problems/one/*.java com/kasidis/problems/two/*.java com/kasidis/problems/three/*.java com/kasidis/utils/*.java

RUN java com/kasidis/problems/one/ProblemOneMain
RUN java com/kasidis/problems/two/ProblemTwoMain
RUN java com/kasidis/problems/three/ProblemThreeMain
